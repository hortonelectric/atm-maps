var webpack = require('webpack');

module.exports = {
	context: __dirname + '/src',
	entry: {
		app: './app.js',
		vendor: ['angular']
	},
	output: {
		path: __dirname + '/build',
		filename: 'app.bundle.js'
	},
	module : {
		loaders:[
			{ test: /\.html$/, loader: "html" },
			{ test: /\.json$/, loader: "json" }
		]	
	},
	plugins: [
		new webpack.optimize.CommonsChunkPlugin(/* chunkName= */"vendor", /* filename= */"vendor.bundle.js")
	]
};
