var path = require('path');
var express = require('express');

var app = express();
var port = 8000;

app.use('/bower_components',  express.static(__dirname + '/bower_components'));
app.use('/build',  express.static(__dirname + '/build'));
app.use('/less',  express.static(__dirname + '/less'));
app.use('/img',  express.static(__dirname + '/img'));
app.use('/src/js',  express.static(__dirname + '/src/js'));

app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname, 'index.html'));
});

app.listen(port, '0.0.0.0', function(err) {
    if (err) {
	        console.log(err);
	        return;
	    }
    console.info('  ==> 🌎Server on port %s. Open up http://localhost:%s/ in your browser', port, port)
});
