var preview = require('../../js/preview')

var icon1 = {
	url: "../../../img/map_marker_1.ico", // url
	scaledSize: new google.maps.Size(40, 40), // scaled size
	origin: new google.maps.Point(0,0), // origin
	anchor: new google.maps.Point(0, 0) // anchor
};

var icon2 = {
	url: "../../../img/map_marker_2.ico", // url
	scaledSize: new google.maps.Size(40, 40), // scaled size
	origin: new google.maps.Point(0,0), // origin
	anchor: new google.maps.Point(0, 0) // anchor
};

var search_icon = {
	url: "../../../img/map_marker_3.ico", // url
	scaledSize: new google.maps.Size(32, 32), // scaled size
	origin: new google.maps.Point(0,0), // origin
	anchor: new google.maps.Point(0, 0) // anchor
};

var location_icon = {
	url: "../../../img/map_marker_4.ico", // url
	scaledSize: new google.maps.Size(32, 32), // scaled size
	origin: new google.maps.Point(0,0), // origin
	anchor: new google.maps.Point(0, 0) // anchor
};

var initMap = function(markers) {

	var infowindow;
	var map;
	var cluster = [];

	map = new google.maps.Map(document.getElementById('map'), {
		center: {lat: 40, lng: 0},
		zoom: 2,
	});

	infowindow = new google.maps.InfoWindow();

	for (var key in markers) {

		var marker = new google.maps.Marker({
			position: {lat: parseFloat(markers[key].location.lat), lng: parseFloat(markers[key].location.long) },
			map: map,
			icon: markers[key].atm.direction == 1 ? icon1 : icon2
		});
		
		google.maps.event.addListener(marker, 'click', (function(marker, profile) {
			return function() {
				infowindow.setContent(preview(profile));
				infowindow.open(map, marker);
			}
		}) (marker, markers[key]));

		cluster.push(marker);
	}

	var markerCluster = new MarkerClusterer(map, cluster, 
		{ imagePath: '../../../bower_components/js-marker-clusterer/images/m' }
	);
	
	//Autocomplete
	var input = /** @type {!HTMLInputElement} */(document.getElementById('address'));


	var search_marker = new google.maps.Marker({
		map: map,
		animation: google.maps.Animation.DROP,
		icon: search_icon
	});

	var location_marker = new google.maps.Marker({
		map: map,
		animation: google.maps.Animation.BOUNCE,
		icon: location_icon
	});

	map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
	input.className = "controls";

	var autocomplete = new google.maps.places.Autocomplete(input);
	autocomplete.bindTo('bounds', map);

	input.onkeypress = function(e) {
		if (!e) e = window.event;
		var keyCode = e.keyCode || e.which;

		if (keyCode == '13'){

			var geocoder = new google.maps.Geocoder();
			geocoder.geocode({address: input.value}, function(results, status) {

				if (status == google.maps.GeocoderStatus.OK) {
					var coordinates = results[0].geometry.location;

					map.setCenter(coordinates);
					map.setZoom(10);

					search_marker.setVisible(false);
					search_marker.setPosition(coordinates);
					search_marker.setVisible(true);

					$('#address').blur();
				}else{
					window.alert('No Location Detected!');
				}
			});

		}
	}

	if (navigator.geolocation) {

		navigator.geolocation.getCurrentPosition(function(position) {
			var coordinates = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
			};

			location_marker.setVisible(false);
			location_marker.setPosition(coordinates);
			location_marker.setVisible(true);

			map.setCenter(coordinates);
			map.setZoom(10);
		}, function(error) {
			console.log(error)
		});

	} else {
		window.alert('Your browser does not support location detection');
	}

}


module.exports = initMap;
