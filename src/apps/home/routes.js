var routes = function ($stateProvider) {
	$stateProvider
		.state('home', {
			url: '/',
			template: require('./index.html')
		});
}

routes.$inject = ['$stateProvider'];

module.exports = routes;
