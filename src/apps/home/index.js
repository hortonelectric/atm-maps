var angular = require('angular');
var uirouter = require('angular-ui-router');
var HomeController = require('./HomeController');
var routing = require('./routes');

module.exports = angular.module('app.home', [uirouter])
	.controller('HomeController',HomeController)
	.config(routing)
	.name;
