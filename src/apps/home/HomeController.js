var maps = require('./map')
var config = require('../../../config.json')

var api = config.current
var mapFilter = require('../../js/mapFilter')

var HomeController = function ($rootScope, $scope, $http) {

	$scope.fullscreen = false;

	$http({
		method: 'GET',
		url: api + 'atm'
	}).then(function(response) {
		maps(mapFilter(response.data.data))
	}, function(error) {
		console.log(error);
	});
};

HomeController.$inject = [ '$rootScope', '$scope', '$http'];

module.exports = HomeController;

