var _  = require('lodash')
var config = require('../../../../config.json')

var maps = require('../maps')
var mapFilter = require('../../../js/mapFilter')
var api = config.current

module.exports = function($scope, $stateParams, $http, cb) {
	
	$http({
		method: 'GET',
		url: api + 'atm/' + $stateParams.id + '/operator'
	}).then(function(response) {
		cb(response.data)
		maps(response.data)
	}, function(error) {
		console.log(error);
	});

}
