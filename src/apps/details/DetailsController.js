var config = require('../../../config.json')

var _  = require('lodash')
var getATMDetails = require('./middleware/api')

var DetailsController = function ($rootScope, $scope, $stateParams, $http, $sce) {

	getATMDetails($scope, $stateParams, $http, function(details){
		console.log(details)
		$scope.details = details 
		$scope.id = $stateParams.id
		$scope.address = $sce.trustAsHtml(address(details.location.address)) 
		$scope.openHours = details.location.openHours ? $sce.trustAsHtml(details.location.openHours) : 'N/A'
		meta($rootScope, details)
	})
}

var address = function(address){

	var street = address.street || '';
	var city = address.city;
	var country = address.country;
	var comma = address.zip || address.state ? ', ' : '';

	if(country == "United States" || country == "Canada"){

		var state = address.state ? address.state + ' ' : '';
		var zip = address.zip || '';

		return 	street + '<br />' +
			city + comma + state + zip + '<br />' +
			country;

	}else if(country == "United Kingdom"){
		
		var state = address.state ? address.state + ' ' : '';
		var zip = address.zip || '';

		return 	street + '<br />' +
			city +  ',<br />' + 
			state + zip + ',<br />' +
			country;

	}else if(country == "Spain" || country == "Finland"){
		
		var state = address.state ? ', ' + address.state : '';
		var zip = address.zip ? address.zip + ' ' : '';

		return 	street + '<br />' +
			zip + city + state + '<br />' + 
			country;

	}else if(country == "Australia"){
		
		var state = address.state ?  ' ' + address.state : '';
		var zip = address.zip ? ' ' + address.zip : '';

		return 	street + '<br />' +
			city + state + zip + '<br />' +
			country;

	}else{

		var state = address.state ? ', ' + address.state : '';
		var zip = address.zip ? address.zip + ' ' : '';

		return 	street + '<br />' +
			zip + city + state + '<br />' + 
			country;
	}

}

var meta = function($rootScope, details) {
	$rootScope.metadata = {
		title: "Bitcoin ATM in " + details.location.address.city + " - " + details.location.location,
		description: "Find location of " + details.atm.type + " Bitcoin ATM machine in " + details.location.address.city + " at " + details.location.address.street + ", " + details.location.address.city + ", " + details.location.address.state + " " + details.location.address.zip + ", " + details.location.address.country,
		keywords: "Bitcoin, Location, ATM",
		author: "6flag Ventures",
		twitter: {
			card: "summary",
			site: "@CoinATMRadar",
			title: "Bitcoin ATM in " + details.location.address.city + " - " + details.location.location,
			description: "Find location of " + details.atm.type + " Bitcoin ATM machine in " + details.location.address.city + " at " + details.location.address.street + ", " + details.location.address.city + ", " + details.location.address.state + " " + details.location.address.zip + ", " + details.location.address.country
		},
		og: {
			title: "Bitcoin ATM in " + details.location.address.city + " - " + details.location.location,
			article: "article",
			description: "Find location of " + details.atm.type + " Bitcoin ATM machine in " + details.location.address.city + " at " + details.location.address.street + ", " + details.location.address.city + ", " + details.location.address.state + " " + details.location.address.zip + ", " + details.location.address.country,
			url: config.website + "/details/" + details._id,
			image: "/"
		}
	};
}

DetailsController.$inject = [ '$rootScope','$scope', '$stateParams', '$http', '$sce']
module.exports = DetailsController;
