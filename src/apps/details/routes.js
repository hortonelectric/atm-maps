var routes = function ($stateProvider) {
	$stateProvider
		.state('details', {
			url: '/details/:id',
			template: require('./index.html')
		});
}

routes.$inject = ['$stateProvider'];

module.exports = routes;
