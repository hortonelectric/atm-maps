var initMap = function(location) {

	var map;
	var infowindow;
	var latLngMarker;

	infowindow = new google.maps.InfoWindow();
	latLngMarker = new google.maps.LatLng( parseFloat(location.location.lat), parseFloat(location.location.long));

	map = new google.maps.Map(document.getElementById('map-atm'), {
		center: {lat: parseFloat(location.location.lat), lng: parseFloat(location.location.long)},
		zoom: 17,
	});

	var marker = new google.maps.Marker({
		position: latLngMarker,
		map: map,
	});

	google.maps.event.addListener(marker, 'click', (function(marker, key) {
		return function() {
			infowindow.setContent(require('../../../maps/details_html/' + key + '.html'));
			infowindow.open(map, marker);
		}
	}) (marker, location.id));

}

module.exports = initMap;
