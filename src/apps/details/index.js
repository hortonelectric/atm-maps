var angular = require('angular');
var uirouter = require('angular-ui-router');
var routing = require('./routes');
var DetailsController = require('./DetailsController')

module.exports = angular.module('app.details', [uirouter])
	.controller('DetailsController',DetailsController)
	.config(routing)
	.name;
