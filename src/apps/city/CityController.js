var config = require('../../../config.json')

var _  = require('lodash')
var getLocationATM = require('./middleware/api')

var CityController = function ($rootScope, $scope, $stateParams, $state, $http) {

	getLocationATM($scope, $stateParams, $http, function(ATMList, country){

		$scope.ATMList = ATMList
		$scope.city = $stateParams.city
		$scope.total = getTotalATM(ATMList)
		meta($rootScope, {city: $stateParams.city, country: country})

		$scope.navigate = function(details){
			$state.go('details', {id: details._id})
		}

	})

}

var meta = function($rootScope, data) {
	$rootScope.metadata = {
		title: "Bitcoin ATM " + data.city + " " + data.country,
		description: "Find Bitcoin ATM in " + data.city + ", " + data.country + ". The easiest way to buy and sell bitcoins in " + data.city + ".",
		keywords: "Bitcoin, Location, ATM",
		author: "6flag Ventures",
		twitter: {
			card: "summary",
			site: "@CoinATMRadar",
			title: "Bitcoin ATM " + data.city + " " + data.country,
			description: "Find Bitcoin ATM in " + data.city + ", " + data.country + ". The easiest way to buy and sell bitcoins in " + data.city + "."
		},
		og: {
			title:  "Bitcoin ATM " + data.city + " " + data.country,
			article: "article",
			description: "Find Bitcoin ATM in " + data.city + ", " + data.country + ". The easiest way to buy and sell bitcoins in " + data.city + ".",
			url: config.website + "/city/" + data.city,
			image: "/"
		}
	};
}

var getTotalATM = function(ATMList){
	var total = 0

	_.forOwn(ATMList, function(value, key){
		total ++
	})

	return total
}

CityController.$inject = [ '$rootScope', '$scope', '$stateParams', '$state', '$http']
module.exports = CityController;
