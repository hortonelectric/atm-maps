var _  = require('lodash')
var config = require('../../../../config.json')

var maps = require('../maps')
var mapFilter = require('../../../js/mapFilter')
var api = config.current

module.exports = function($scope, $stateParams, $http, cb) {


	$http({
		method: 'GET',
		url: api + 'atm' + '?data=location.address.city:' + $stateParams.city
	}).then(function(response) {
		
		var locations = mapFilter(response.data.data)
		var country = locations[0].location.address.country
		cb(formatData(locations, $stateParams), country)
		maps(locations)

	}, function(error) {
		console.log(error);
	});
}

var formatData = function(location, $stateParams){

	var data = {} 

	_.forOwn(location, function(value, key){
		if($stateParams.city == value.location.address.city){
			data[key] = value
		}
	})
		

	return data
}
