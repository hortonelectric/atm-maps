var routes = function ($stateProvider) {
	$stateProvider
		.state('city', {
			url: '/city/:city',
			template: require('./index.html')
		});
}

routes.$inject = ['$stateProvider'];

module.exports = routes;
