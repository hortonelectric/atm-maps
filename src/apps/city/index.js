var angular = require('angular');
var uirouter = require('angular-ui-router');
var routing = require('./routes');
var CityController = require('./CityController.js')

module.exports = angular.module('app.city', [uirouter])
	.controller('CityController', CityController)
	.config(routing)
	.name;
