var angular = require('angular');
var uirouter = require('angular-ui-router');12
var routing = require('./routes');

module.exports = angular.module('app.tutorial', [uirouter])
	.config(routing)
	.name;