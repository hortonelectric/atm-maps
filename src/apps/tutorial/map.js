var initMap = function(markers) {


	var map;
	var infowindow;
	var cluster = [];

	map = new google.maps.Map(document.getElementById('map'), {
		center: {lat: 40, lng: 0},
		zoom: 2,
	});

	infowindow = new google.maps.InfoWindow();

	for (var key in markers) {
		var marker = new google.maps.Marker({
			position: {lat: parseFloat(markers[key].location.lat), lng: parseFloat(markers[key].location.long) },
			map: map,
		});
		
		google.maps.event.addListener(marker, 'click', (function(marker, key) {
			return function() {
				infowindow.setContent(require('../../../maps/details_html/' + key + '.html'));
				infowindow.open(map, marker);
			}
		}) (marker, markers[key].id));

		cluster.push(marker);
	}

	var markerCluster = new MarkerClusterer(map, cluster, 
		{ imagePath: '../../../bower_components/js-marker-clusterer/images/m' }
	);
}

module.exports = initMap;
