var maps = require('./map')
var config = require('../../../config.json')

var api = config.current
var mapFilter = require('../../js/mapFilter')

var HomeController = function ($scope, $http) {

	$http({
		method: 'GET',
		url: api + 'atm'
	}).then(function(response) {
		maps(mapFilter(response.data.data))
	}, function(error) {
		console.log(error);
	});

};

HomeController.$inject = [ '$scope', '$http'];

module.exports = HomeController;

