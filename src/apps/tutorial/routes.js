var routes = function ($stateProvider) {
	$stateProvider
		.state('tutorial', {
			url: '/tutorial',
			template: require('./index.html')
		})
		.state('tutorial.buy_bitcoins_with_atm', {
			url: '/buy-bitcoins-with-atm',
			template: require('./tpl/buy_bitcoins_with_atm.html')
		})
		.state('tutorial.sell_bitcoins_with_atm', {
			url: '/sell-bitcoins-with-atm',
			template: require('./tpl/sell_bitcoins_with_atm.html')
		})
		.state('tutorial.withdraw_bitcoins_with_atm', {
			url: '/withdraw-bitcoin-from-receipt',
			template: require('./tpl/withdraw_bitcoin_from_receipt.html')
		})
		.state('tutorial.buy_bitcoins_to_hardware_wallet', {
			url: '/buy-bitcoins-to-hardware-wallet',
			template: require('./tpl/buy_bitcoins_to_hardware_wallet.html')
		})
		.state('tutorial.alternative_exchange_services', {
			url: '/alternative-exchange-services',
			template: require('./tpl/alternative_exchange_services.html')
		})
		.state('tutorial.running_bitcoin_atm', {
			url: '/running-bitcoin-atm',
			template: require('./tpl/running_bitcoin_atm.html')
		})
		.state('tutorial.bitcoin_atm_metric', {
			url: '/bitcoin-atm-metric',
			template: require('./tpl/bitcoin_atm_metric.html')
		});
}

routes.$inject = ['$stateProvider'];

module.exports = routes;