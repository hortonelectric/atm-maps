var angular = require('angular');
var uirouter = require('angular-ui-router');
var NavigationDirective = require('./NavigationDirective');

module.exports =
	angular.module('app.navigation', [])
		.directive('appNavigation', NavigationDirective)
		.name;
