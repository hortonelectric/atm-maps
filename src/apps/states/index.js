var angular = require('angular');
var uirouter = require('angular-ui-router');
var routing = require('./routes');
var StatesController = require('./StatesController.js')

module.exports = angular.module('app.states', [uirouter])
	.controller('StatesController', StatesController)
	.config(routing)
	.name;
