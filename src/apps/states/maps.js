var preview = require('../../js/preview')
var initMap = function(markers) {

	var map;
	var infowindow;
	var markerBounds;
	var latLngMarker;
	var cluster = [];

	map = new google.maps.Map(document.getElementById('map_states'), {
		center: {lat: 40, lng: 0},
		zoom: 2,
	});

	infowindow = new google.maps.InfoWindow();
	markerBounds = new google.maps.LatLngBounds();

	for (var key in markers) {
		
		latLngMarker = new google.maps.LatLng( parseFloat(markers[key].location.lat), parseFloat(markers[key].location.long));
		
		var marker = new google.maps.Marker({
			position: latLngMarker,
			map: map,
		});
		
		google.maps.event.addListener(marker, 'click', (function(marker, profile) {
			return function() {
				infowindow.setContent(preview(profile));
				infowindow.open(map, marker);
			}
		}) (marker, markers[key]));

		cluster.push(marker);
		markerBounds.extend(latLngMarker);
	}

	var markerCluster = new MarkerClusterer(map, cluster, 
		{ imagePath: '../../../bower_components/js-marker-clusterer/images/m' }
	);

	map.fitBounds(markerBounds);
}

module.exports = initMap;
