var routes = function ($stateProvider) {
	$stateProvider
		.state('states', {
			url: '/states/:state/:longState',
			template: require('./index.html')
		});
}

routes.$inject = ['$stateProvider'];

module.exports = routes;
