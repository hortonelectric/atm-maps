var _  = require('lodash')
var config = require('../../../../config.json')

var maps = require('../maps')
var mapFilter = require('../../../js/mapFilter')
var api = config.current

module.exports = function($scope, $stateParams, $http, cb){
	
	$http({
		method: 'GET',
		url: api + 'atm' + '?data=location.address.state:' + $stateParams.state
	}).then(function(response) {

		var locations = mapFilter(response.data.data)
		var country = locations[0].location.address.country
		cb(formatData(locations, $stateParams), country )
		maps(locations)

	}, function(error) {
		console.log(error);
	});

}

var formatData = function(location, $stateParams) {
	var cityArray = [] 
	var locations = {}
	var cityObject = {}

	_.forOwn(location, function(value, key){
		if($stateParams.state == value.location.address.state) {
			cityArray.push(value.location.address.city)	
			locations[key] = value
		}
	})

	var uniqueCityArray = getUniqueCities(cityArray)
	
	_.forEach(uniqueCityArray,function(value){
		cityObject[value] = getTotalCity(value, locations)
	})

	return cityObject
}

var getUniqueCities = function(cityArray){
	var uniqueCities = _.uniqBy(cityArray) 
	return uniqueCities
}

var getTotalCity = function(city, locations){
	var total = 0

	_.forOwn(locations, function(value,key){
		if(city == value.location.address.city){
			total++
		}	
	})
	
	return total
}
