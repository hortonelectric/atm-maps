var config = require('../../../config.json')

var _  = require('lodash')
var getLocationCities = require('./middleware/api')

var StatesController = function ($rootScope, $scope, $stateParams, $http) {

	getLocationCities($scope, $stateParams, $http, function(cityList, country){
		$scope.cityList = cityList
		$scope.state = $stateParams.longState
		$scope.country = country	
		$scope.total = getTotalATM(cityList)
		console.log($rootScope)
		meta($rootScope, {state: $stateParams.state, longState: $stateParams.longState, country: country})
	})

}

var getTotalATM = function(cityList){
	var total = 0

	_.forOwn(cityList, function(value, key){
		total += parseInt(value)
	})

	return total
}

var meta = function($rootScope, data) {
	$rootScope.metadata = {
		title: "Bitcoin ATM " + data.longState + " " + data.country,
		description: "Find Bitcoin ATM in " + data.longState + ", " + data.country + ". The easiest way to buy and sell bitcoins in " + data.longState + ".",
		keywords: "Bitcoin, Location, ATM",
		author: "6flag Ventures",
		twitter: {
			card: "summary",
			site: "@CoinATMRadar",
			title: "Bitcoin ATM " + data.longState + " " + data.country,
			description: "Find Bitcoin ATM in " + data.longState + ", " + data.country + ". The easiest way to buy and sell bitcoins in " + data.longState + "."
		},
		og: {
			title:  "Bitcoin ATM " + data.longState + " " + data.country,
			article: "article",
			description: "Find Bitcoin ATM in " + data.longState + ", " + data.country + ". The easiest way to buy and sell bitcoins in " + data.longState + ".",
			url: config.website + "/states/" + data.state + '/' + data.longState,
			image: "/"
		}
	};
}

StatesController.$inject = [ '$rootScope', '$scope', '$stateParams', '$http']
module.exports = StatesController;
