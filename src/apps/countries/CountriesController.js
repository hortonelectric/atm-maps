var _  = require('lodash')
var states = require('../../../maps/states.json')
var geLocationStates = require('./middleware/api')

var CountriesController = function ($scope, $stateParams, $http) {

	geLocationStates($scope,$stateParams, $http, states, function(stateList){
		$scope.stateList = stateList
		$scope.country = $stateParams.country
		$scope.total = getTotalATM(stateList)
	})

}

var getTotalATM = function(stateList){
	var total = 0
	
	_.forOwn(stateList, function(value, key){
		total += parseInt(value.total)
	})
	
	return total
}
CountriesController.$inject = [ '$scope', '$stateParams', '$http']
module.exports = CountriesController;
