var angular = require('angular');
var uirouter = require('angular-ui-router');
var routing = require('./routes');
var CountriesController = require('./CountriesController.js')

module.exports = angular.module('app.countries', [uirouter])
	.controller('CountriesController', CountriesController)
	.config(routing)
	.name;
