var _  = require('lodash')
var config = require('../../../../config.json')

var maps = require('../maps')
var mapFilter = require('../../../js/mapFilter')
var api = config.current

module.exports = function($scope, $stateParams, $http, states, cb){
	
	$http({
		method: 'GET',
		url: api + 'atm' + '?data=location.address.country:' + $stateParams.country
	}).then(function(response) {

		var locations = mapFilter(response.data.data)
		cb(formatData(locations, states))
		maps(locations)

	}, function(error) {
		console.log(error);
	});

}

var formatData = function(location, states){

	var data = {} 

	_.forOwn(states, function(value, key){
		var total = 0
		_.forOwn(location, function(locationValue, locationKey){
			if(key == locationValue.location.address.state){
				total++	
			}
		})
		total !== 0 ? data[key] = {state: value, total: total} : null
	})

	return data
} 
