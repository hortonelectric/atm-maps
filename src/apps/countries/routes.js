var config = require('../../../config.json');

var routes = function ($stateProvider) {
	$stateProvider
		.state('countries', {
			url: '/countries/:country',
			template: require('./index.html'),
			onEnter: function($rootScope, $stateParams) {
				$rootScope.metadata = {
					title: "Bitcoin ATM " + $stateParams.country + " – Find Bitcoin ATM",
					description: "Locations of Bitcoin ATM in " + $stateParams.country + " The easiest way to buy and sell bitcoins.",
					keywords: "Bitcoin, Location, ATM",
					author: "6flag Ventures",
					twitter: {
						card: "summary",
						site: "@CoinATMRadar",
						title: "Bitcoin ATM " + $stateParams.country + " – find bitcoin machine locations",
						description: "Locations of Bitcoin ATM in " + $stateParams.country + " The easiest way to buy and sell bitcoins."
					},
					og: {
						title: "Bitcoin ATM United States – find bitcoin machine locations",
						article: "article",
						description: "Locations of Bitcoin ATM in United States The easiest way to buy and sell bitcoins.",
						url: config.website + "/countries/" + $stateParams.country,
						image: "/"
					}
				};
			}
		});
}

routes.$inject = ['$stateProvider'];

module.exports = routes;
