module.exports = function (data) {
	return '<div class="marker-info">' + 
				'<div class="marker-title">' + data.location.address.city + '</div>' +
				'<strong>Location:</strong> ' + data.location.location + '<br>' +
				'<strong>Address:</strong><br>' + 
				address(data.location.address) + '<br>' +
				'<strong>Open hours:</strong><br>' + 
				data.location.openHours + '<br>' + 
				'<strong>Direction:'+ direction(data.atm.direction) + '</strong><br> ' +
				'<strong>Supported coins:</strong> ' + 
				supported(data.atm.supported) + '<br>' + 
				'<strong>Fees:</strong> ' + data.atm.fees + '<br>' + 
				'<strong>Limits:</strong> ' + data.atm.limits + '<br>' + 
				'<strong>ATM Type:</strong> ' + data.atm.type + '<br>' + 
				'<a target="_blank" href="/details/' + data._id + '">Read more →</a>' +
			'</div>'
}

var direction = function (direction){
	return direction == 0 ? 'Fiat → Crypto' : 'Fiat ⇄ Crypto'
}

var address = function(address){

	var street = address.street || '';
	var city = address.city;
	var country = address.country;
	var comma = address.zip || address.state ? ', ' : '';

	if(country == "United States" || country == "Canada"){

		var state = address.state ? address.state + ' ' : '';
		var zip = address.zip || '';

		return 	street + '<br />' +
			city + comma + state + zip + '<br />' +
			country;

	}else if(country == "United Kingdom"){
		
		var state = address.state ? address.state + ' ' : '';
		var zip = address.zip || '';

		return 	street + '<br />' +
			city +  ',<br />' + 
			state + zip + ',<br />' +
			country;

	}else if(country == "Spain" || country == "Finland"){
		
		var state = address.state ? ', ' + address.state : '';
		var zip = address.zip ? address.zip + ' ' : '';

		return 	street + '<br />' +
			zip + city + state + '<br />' + 
			country;

	}else if(country == "Australia"){
		
		var state = address.state ?  ' ' + address.state : '';
		var zip = address.zip ? ' ' + address.zip : '';

		return 	street + '<br />' +
			city + state + zip + '<br />' +
			country;

	}else{

		var state = address.state ? ', ' + address.state : '';
		var zip = address.zip ? address.zip + ' ' : '';

		return 	street + '<br />' +
			zip + city + state + '<br />' + 
			country;
	}
}

var supported = function (supported){
	return supported.map(function(coin){
		return '<img src="/img/' + coin.toLowerCase() + '_small.png" /> '
	})
}
