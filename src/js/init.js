var config = require('../../config.json');

var initialize = function ($rootScope) {
	$rootScope.metadata = {
		title: "BTM Radar – Find Bitcoin ATM",
		description: "Find Bitcoin ATM locations easily with our Bitcoin ATM Map. For many Bitcoin machines online rates are available.",
		keywords: "Bitcoin, Location, ATM",
		author: "6flag Ventures",
		twitter: {
			card: "summary",
			site: "@BTMradar",
			title: "BTM Radar – Find Bitcoin ATM, Online Rates",
			description: "Find Bitcoin ATM locations easily with our Bitcoin ATM Map. For many Bitcoin machines online rates are available."
		},
		og: {
			title: "BTM Radar – Find Bitcoin ATM, Online Rates",
			article: "article",
			description: "Find Bitcoin ATM locations easily with our Bitcoin ATM Map. For many Bitcoin machines online rates are available.",
			url: config.website,
			image: "/"
		}
	};
} 

initialize.$inject = ['$rootScope'];

module.exports = initialize;
