$(function() {
	$("#learnMoreBtn").click(function() {
		$('#basicTutorial').collapse('toggle');
	});
	$('#basicTutorial').on('shown.bs.collapse', function () {
		$('html,body').animate({
			scrollTop: $("#basicTutorial").offset().top
		},'slow');
		$("#learnMoreBtn").remove();
	})
});
