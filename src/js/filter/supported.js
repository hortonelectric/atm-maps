module.exports = function() {
	return function(data){
		return data ?  data.join(', ')	: 'N/A'
	}
}
