var supportedAtm = require('./supported')

module.exports = angular.module('app', [])
	.filter('supportedAtm', supportedAtm);

