var supportedAtm = require('./js/filter/supported');
var directionAtm = require('./js/filter/direction');


var angular = require('angular');
var uirouter = require('angular-ui-router');

var config = require('./app.config');
var initialize = require('./js/init')

var home = require('./apps/home/index');
var navigation =  require('./apps/navigation/index');
var details = require('./apps/details/index');
var countries = require('./apps/countries/index');
var states = require('./apps/states/index');
var city = require('./apps/city/index');

var tutorial = require('./apps/tutorial/index');

angular.module('app', [
		uirouter,

		home, 
		navigation,
		details,
		countries,
		states,
		city,
		tutorial
	])
	.filter('supportedAtm', supportedAtm)
	.filter('directionAtm', directionAtm)
	.config(config)
	.run(initialize);
