var config = function ($urlRouterProvider, $locationProvider) {
	$locationProvider.html5Mode(true);
	$urlRouterProvider.otherwise('/');
} 

config.$inject = ['$urlRouterProvider', '$locationProvider'];

module.exports = config;
