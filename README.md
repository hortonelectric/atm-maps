# Installation

STEP 1 - You need these installed on your system.

Requirements:

- Node js - https://nodejs.org/en/download/ (Download here or install with your package manager).
- NPM - Comes with node js when installed.
- Bower - https://bower.io/#install-bower (If nodejs is installed just type "npm install -g bower")

STEP 2 - Clone the repo

```sh
$ git clone git@bitbucket.org:betcn/atm-maps-ng.git
```

STEP 3 - Go to the repo folder and install the application dependencies.

```sh
$ cd atm-maps-ng
$ npm install
$ bower install
```

Step 4 - Install webpack globally. Use sudo if prompt for admin.

```sh
$ npm install -g webpack
(or)
$ sudo npm install -g webpack
```


Step 5 - Go to bower_components folder and clone the marker cluster dependency

```sh
$ cd bower_components
$ git clone https://github.com/googlemaps/js-marker-clusterer.git
```

Step 6 - Build the application and run node

```sh
$ cd .. (if you are not yet at the index of the app)
$ webpack
$ node .
```
